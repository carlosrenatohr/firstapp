# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
FirstApp::Application.config.secret_key_base = 'f9f7249831d778c10caccbccdaf66ccaf144e0d0c23dd199e13d9dee98aab1d2198a91bf3265de4216f6a7bcb68b1aa3cb1159a19a45cdb7f20c9e76a626ac71'
